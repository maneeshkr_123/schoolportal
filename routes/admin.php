<?php

Route::get('/', 'IndexController@index');

Route::get('/faculties', 'FacultyController@index');
Route::get('/faculty/create', 'FacultyController@create');
Route::post('/faculty/create', 'FacultyController@store');
Route::get('/faculty/edit', 'FacultyController@edit');
Route::put('/faculty/edit', 'FacultyController@update');
Route::get('/faculty/delete', 'FacultyController@delete');
